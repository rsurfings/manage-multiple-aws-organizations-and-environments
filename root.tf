provider "aws" {
	region = "us-west-2"
	access_key = ""
	secret_key = ""
}

resource "aws_s3_bucket" "edge" {
  bucket = "edge"
  acl = "private"
  
  versioning {
    enabled = true
  }
  tags = {
    Name        = "My bucket"
    Environment = "edge"
  }
}

resource "aws_s3_bucket" "dev" {
  bucket = "dev"
  acl = "private"
  
  versioning {
    enabled = true
  }
  tags = {
    Name        = "My bucket"
    Environment = "dev"
  }
}


resource "aws_s3_bucket" "stg" {
  bucket = "stg"
  acl = "private"
  
  versioning {
    enabled = true
  }
  tags = {
    Name        = "My bucket"
    Environment = "stg"
  }
}


resource "aws_s3_bucket" "readys3" {
  bucket = "readys3"
  acl = "private"
  
  versioning {
    enabled = true
  }
  tags = {
    Name        = "My bucket"
    Environment = "readys3"
  }
}


resource "aws_s3_bucket" "lives3" {
  bucket = "lives3"
  acl = "private"
  
  versioning {
    enabled = true
  }
  tags = {
    Name        = "My bucket"
    Environment = "lives3"
  }
}
